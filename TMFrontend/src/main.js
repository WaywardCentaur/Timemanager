import './assets/main.css'

import "../node_modules/bootstrap/dist/css/bootstrap.css";

import "../node_modules/bootstrap/dist/js/bootstrap.bundle";

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

router.beforeEach((to, from, next) => {
  const userStorage = localStorage.getItem('user');
  // If logged in, or going to the Login page.
  if (userStorage || to.name === 'login' || to.name === 'register') {
    // Continue to page.
    next()
  } else {
    // Not logged in, redirect to login.
    next({ name: 'login' })
  }
});

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')

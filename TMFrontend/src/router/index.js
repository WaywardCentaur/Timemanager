import { createRouter, createWebHistory } from 'vue-router'

import ChartManager from '../components/ChartManager.vue'
import WorkingtimeManager from '../components/WorkingtimeManager.vue'
import User from '../components/User.vue'
import ClockManager from '../components/ClockManager.vue'
import LoginOrRegister from '../components/LoginOrRegister.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: LoginOrRegister
    },
    {
      path: '/',
      name: 'chartManager',
      component: ChartManager
    },
    {
      path: '/workingtime/:userId',
      name: 'createWorkingTime',
      component: WorkingtimeManager
    },
    {
      path: '/workingtime/:userid/:workingTimeId',
      name: 'changeWorkingTime',
      component: WorkingtimeManager
    },
    {
      path: '/user',
      name: 'user',
      component: User
    },
    {
      path: '/user/:userId',
      name: 'userupdate',
      component: User
    },
    {
      path: '/users',
      name: 'users',
      component: User
    },
    {
      path: '/clock/:username',
      name: 'clock',
      component: ClockManager
    },
    {
      path: '/register',
      name: 'register',
      component: LoginOrRegister
    }

  ]
})

export default router

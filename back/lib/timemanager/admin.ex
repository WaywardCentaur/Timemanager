defmodule Timemanager.Admin do
  @moduledoc """
  The Admin context.
  """

  import Ecto.Query, warn: false
  alias Timemanager.Repo

  alias Timemanager.Admin.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  alias Timemanager.Admin.Time

  @doc """
  Returns the list of clock.

  ## Examples

      iex> list_clock()
      [%Time{}, ...]

  """
  def list_clock do
    Repo.all(Time)
  end

  @doc """
  Gets a single time.

  Raises `Ecto.NoResultsError` if the Time does not exist.

  ## Examples

      iex> get_time!(123)
      %Time{}

      iex> get_time!(456)
      ** (Ecto.NoResultsError)

  """
  def get_time!(id), do: Repo.get!(Time, id)

  @doc """
  Creates a time.

  ## Examples

      iex> create_time(%{field: value})
      {:ok, %Time{}}

      iex> create_time(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_time(attrs \\ %{}) do
    %Time{}
    |> Time.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a time.

  ## Examples

      iex> update_time(time, %{field: new_value})
      {:ok, %Time{}}

      iex> update_time(time, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_time(%Time{} = time, attrs) do
    time
    |> Time.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a time.

  ## Examples

      iex> delete_time(time)
      {:ok, %Time{}}

      iex> delete_time(time)
      {:error, %Ecto.Changeset{}}

  """
  def delete_time(%Time{} = time) do
    Repo.delete(time)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking time changes.

  ## Examples

      iex> change_time(time)
      %Ecto.Changeset{data: %Time{}}

  """
  def change_time(%Time{} = time, attrs \\ %{}) do
    Time.changeset(time, attrs)
  end

  alias Timemanager.Admin.Intervals

  @doc """
  Returns the list of workingtimes.

  ## Examples

      iex> list_workingtimes()
      [%Intervals{}, ...]

  """
  def list_workingtimes do
    Repo.all(Intervals)
  end

  @doc """
  Gets a single intervals.

  Raises `Ecto.NoResultsError` if the Intervals does not exist.

  ## Examples

      iex> get_intervals!(123)
      %Intervals{}

      iex> get_intervals!(456)
      ** (Ecto.NoResultsError)

  """
  def get_intervals!(id), do: Repo.get!(Intervals, id)

  @doc """
  Creates a intervals.

  ## Examples

      iex> create_intervals(%{field: value})
      {:ok, %Intervals{}}

      iex> create_intervals(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_intervals(attrs \\ %{}) do
    %Intervals{}
    |> Intervals.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a intervals.

  ## Examples

      iex> update_intervals(intervals, %{field: new_value})
      {:ok, %Intervals{}}

      iex> update_intervals(intervals, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_intervals(%Intervals{} = intervals, attrs) do
    intervals
    |> Intervals.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a intervals.

  ## Examples

      iex> delete_intervals(intervals)
      {:ok, %Intervals{}}

      iex> delete_intervals(intervals)
      {:error, %Ecto.Changeset{}}

  """
  def delete_intervals(%Intervals{} = intervals) do
    Repo.delete(intervals)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking intervals changes.

  ## Examples

      iex> change_intervals(intervals)
      %Ecto.Changeset{data: %Intervals{}}

  """
  def change_intervals(%Intervals{} = intervals, attrs \\ %{}) do
    Intervals.changeset(intervals, attrs)
  end
end

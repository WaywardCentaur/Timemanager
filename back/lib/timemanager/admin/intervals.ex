defmodule Timemanager.Admin.Intervals do
  use Ecto.Schema
  import Ecto.Changeset

  schema "workingtimes" do
    field :start, :utc_datetime
    field :end, :utc_datetime

    belongs_to :user, Timemanager.Admin.User

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(intervals, attrs) do
    intervals
    |> cast(attrs, [:start, :end, :user])
    |> validate_required([:start, :end, :user])
  end
end

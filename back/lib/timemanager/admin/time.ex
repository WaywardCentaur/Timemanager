defmodule Timemanager.Admin.Time do
  use Ecto.Schema
  import Ecto.Changeset

  schema "clock" do
    field :status, :boolean, default: false
    field :time, :utc_datetime


    belongs_to :user, Timemanager.Admin.User

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(time, attrs) do
    time
    |> cast(attrs, [:time, :status, :user])
    |> validate_required([:time, :status, :user])
  end
end

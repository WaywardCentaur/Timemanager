defmodule Timemanager.Admin.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :blocked, :boolean, default: false
    field :mobile, :string
    field :role, :integer
    field :username, :string
    field :nom, :string
    field :prenom, :string
    field :email, :string
    field :profil, :string
    field :deleteOk, :boolean, default: false
    field :registeredAt, :utc_datetime
    field :lastLogin, :utc_datetime

    has_many :clock, Timemanager.Admin.Intervals
    has_many :workingtimes, Timemanager.Admin.Time
    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:nom, :prenom, :username, :email, :role, :mobile, :profil, :blocked, :deleteOk, :registeredAt, :lastLogin])
    |> validate_required([:nom, :prenom, :username, :email, :role, :mobile, :profil, :blocked, :deleteOk, :registeredAt, :lastLogin])
    |> unique_constraint([:email, :username])
    |> validate_format(:email, ~r/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)
  end
end

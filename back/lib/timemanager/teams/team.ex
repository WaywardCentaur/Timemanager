defmodule Timemanager.Teams.Team do
  use Ecto.Schema
  import Ecto.Changeset

  schema "teams" do
    field :teamNumber, :integer

    timestamps(type: :utc_datetime)

    belongs_to :user, Timemanager.Userbase.User
  end

  @doc false
  def changeset(team, attrs) do
    team
    |> cast(attrs, [:teamNumber])
    |> validate_required([:teamNumber])
  end
end

defmodule Timemanager.Time.Clock do
  use Ecto.Schema
  import Ecto.Changeset

  schema "clocks" do
    field :status, :boolean, default: true
    field :time, :utc_datetime


    belongs_to :user, Timemanager.Userbase.User

    timestamps(type: :utc_datetime, inserted_at: false, updated_at: false)
  end

  @doc false
  def changeset(clock, attrs) do
    clock
    |> cast(attrs, [:time, :status, :user_id])
    |> validate_required([:time, :status, :user_id])
  end
end

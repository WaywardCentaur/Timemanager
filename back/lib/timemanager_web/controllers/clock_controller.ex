defmodule TimemanagerWeb.ClockController do
  use TimemanagerWeb, :controller

  alias Timemanager.Time

  def index(conn, %{"userID" => userID}) do
    clocks = Time.get_clocks_for_user(userID)
    query_params = Enum.into(conn.query_params, %{})

    clocksFiltered = Enum.reduce(query_params, clocks, fn {key, value}, acc_clocks ->
      Enum.filter(acc_clocks, fn clock ->
        Map.get(clock, String.to_existing_atom(key)) == value
      end)
    end)

    render(conn, :index, clocks: clocksFiltered)
  end

  def create(conn, %{"userID" => userID, "clock" => clock_params}) do
    clock_params_with_user_id = Map.put(clock_params, "user_id", String.to_integer(userID))
    case Time.create_clock(clock_params_with_user_id) do
      {:ok, clock} ->
        render(conn, :show, clock: clock)

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :new, changeset: changeset)
    end
  end

  def update(conn, %{"userID" => userID, "clockID" => clockID, "clock" => clock_params}) do
    clock = Time.get_clock!(clockID)
    oldClockTime = clock.time

    case Time.update_clock(clock, clock_params) do
      {:ok, clock} ->
        # create workingtime if new clock status is false (finished)
        if Map.get(clock_params, "status") == false do
          Time.create_working_time(%{
            "start" => oldClockTime,
            "end" => Map.get(clock_params, "time"),
            "user_id" => String.to_integer(userID)
          })
        end
        render(conn, :show, clock: clock)

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :edit, clock: clock, changeset: changeset)
    end
  end

  def delete(conn, %{"userID" => userID}) do
    clock = Time.get_clocks_for_user(userID)
    {:ok, _clock} = Time.delete_clock(clock)

    render(conn, :show, clock: clock)
  end
end

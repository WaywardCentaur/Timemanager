defmodule TimemanagerWeb.ClockJSON do
  alias Timemanager.Time.Clock

  @doc """
  Renders a list of clocks.
  """
  def index(%{clocks: clock}) do
    %{data: for(clock <- clock, do: data(clock))}
  end

  @doc """
  Renders a single clock.
  """
  def show(%{clock: clock}) do
    %{data: data(clock)}
  end

  defp data(%Clock{} = clock) do
    %{
      id: clock.id,
      status: clock.status,
      time: clock.time
    }
  end
end

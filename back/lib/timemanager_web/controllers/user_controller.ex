defmodule TimemanagerWeb.UserController do
  use TimemanagerWeb, :controller

  alias Timemanager.Userbase
  alias Timemanager.Guardian

  def sign_up(conn, %{"user" => user_params}) do
    case Userbase.register_user(user_params) do
      {:ok, user} ->
        {:ok, jwt, _full_claims} = Guardian.encode_and_sign(user, %{})
        Userbase.insert_token(%{
          "token" => jwt,
          "context" => "register",
          "sent_to" => user.username,
          "user_id" => user.id
        })
        render(conn, :showJWT, user: user, jwt: jwt)

      {:error, :invalid_params} ->
        conn
        |> put_status(:bad_request) # HTTP 400 Bad Request
        |> render(:error, message: "Invalid parameters")

      {:error, :duplicate_email} ->
        conn
        |> put_status(:conflict)  # HTTP 409 Conflict
        |> render(:error, message: "Email is already taken")
    end
  end

  # Here user_params contains only email and password
  def sign_in(conn, %{"user" => user_params}) do
    case Userbase.get_user_by_email_and_password(
      Map.get(user_params, "email"),
      Map.get(user_params, "password")
    ) do
      {:ok, user} ->
        jwt = Userbase.get_user_token_with_context(user, ["register"])
        render(conn, :showJWT, user: user, jwt: jwt.token)

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_status(:unauthorized)  # HTTP 401 Unauthorized
        |> render(:error, changeset: changeset)

      {:error, :bad_request} ->
        conn
        |> put_status(:bad_request)  # HTTP 400 Bad Request
        |> render(:error, message: "Bad Request")
    end
  end

  # Here user_params contains only email
  def sign_out(conn, %{"user" => user_params}) do
    case Userbase.delete_all_user_tokens(Map.get(user_params, "email")) do
      {:ok} ->
        conn
        |> put_status(:ok)
        |> send_resp(:ok, "")
        |> halt()

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> render(:error, changeset: changeset)
    end
  end

  def index(conn, _params) do
    users = Userbase.list_users()
    query_params = Enum.into(conn.query_params, %{})

    usersFiltered =
      Enum.reduce(query_params, users, fn {key, value}, acc_users ->
        Enum.filter(acc_users, fn user ->
          Map.get(user, String.to_existing_atom(key)) == value
        end)
      end)

    render(conn, :index, users: usersFiltered)
  end

  def new(conn, _params) do
    render(conn, :new)
  end

  def create(conn, %{"user" => user_params}) do
    case Userbase.create_user(user_params) do
      {:ok, user} ->
        render(conn, :show, user: user)

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :new, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Userbase.get_user!(id)
    render(conn, :show, user: user)
  end

  def edit(conn, %{"id" => id}) do
    user = Userbase.get_user!(id)
    render(conn, :show, user: user)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Userbase.get_user!(id)

    case Userbase.update_user(user, user_params) do
      {:ok, user} ->
        render(conn, :show, user: user)

      {:error, %Ecto.Changeset{} = _changeset} ->
        render(conn, :edit, user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Userbase.get_user!(id)
    {:ok, _user} = Userbase.delete_user(user)

    render(conn, :show, user: user)
  end
end

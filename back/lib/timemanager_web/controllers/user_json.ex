defmodule TimemanagerWeb.UserJSON do
  alias Timemanager.Userbase.User

  @doc """
  Renders a list of users.
  """
  def index(%{users: user}) do
    %{data: for(user <- user, do: data(user))}
  end

  @doc """
  Renders a single user.
  """
  def show(%{user: user}) do
    %{data: data(user)}
  end

  defp data(%User{} = user) do
    %{
      id: user.id,
      username: user.username,
      email: user.email,
      nom: user.nom,
      prenom: user.prenom,
      role: user.role,
    }
  end

  @doc """
  Renders a single user and jwt.
  """
  def showJWT(%{user: user, jwt: jwt}) do
    %{data: dataJWT(user, jwt)}
  end

  defp dataJWT(%User{} = user, jwt) do
    %{
      id: user.id,
      username: user.username,
      email: user.email,
      nom: user.nom,
      prenom: user.prenom,
      role: user.role,
      jwt: jwt
    }
  end
end

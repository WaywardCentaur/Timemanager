defmodule TimemanagerWeb.WorkingTimeController do
  use TimemanagerWeb, :controller
  alias Timemanager.Time

  def create(conn, %{"userID" => userID ,"workingtime" => workingtime_params}) do
    workingtime_params_with_user_id = Map.put(workingtime_params, "user_id", String.to_integer(userID))
    case Time.create_working_time(workingtime_params_with_user_id) do
      {:ok, workingtime} ->
        render(conn, :show, workingtime: workingtime)

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :new, changeset: changeset)
    end
  end

  def index(conn,  %{"userID" => userID}) do
    workingtimes = Time.get_all_working_times_for_user(userID)
    render(conn, :index, workingtimes: workingtimes)
  end

  def show(conn,  %{"userID" => _userID, "workingTimeID" => workingTimeID}) do
    workingtime = Time.get_working_time!(workingTimeID)
    render(conn, :show, workingtime: workingtime)
  end

  def update(conn, %{"userID" => _userID, "workingTimeID" => workingTimeID, "workingtime" => workingtime_params}) do
    workingtime = Time.get_working_time!(workingTimeID)

    case Time.update_working_time(workingtime, workingtime_params) do
      {:ok, workingtime} ->
        render(conn, :show, workingtime: workingtime)

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, :edit, workingtime: workingtime, changeset: changeset)
    end
  end

  def delete(conn, %{"userID" => _userID, "workingTimeID" => workingTimeID}) do
    workingtime = Time.get_working_time!(workingTimeID)
    {:ok, _workingtime} = Time.delete_working_time(workingtime)

    render(conn, :show, workingtime: workingtime)
  end
end

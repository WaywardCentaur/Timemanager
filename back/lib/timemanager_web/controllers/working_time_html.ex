defmodule TimemanagerWeb.WorkingTimeHTML do
  use TimemanagerWeb, :html

  embed_templates "working_time_html/*"

  @doc """
  Renders a working_time form.
  """
  attr :changeset, Ecto.Changeset, required: true
  attr :action, :string, required: true

  def working_time_form(assigns)
end

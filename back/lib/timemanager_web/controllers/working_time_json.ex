defmodule TimemanagerWeb.WorkingTimeJSON do
  alias Timemanager.Time.WorkingTime

  @doc """
  Renders a list of workingtimes.
  """
  def index(%{workingtimes: workingtime}) do
    %{data: for(workingtime <- workingtime, do: data(workingtime))}
  end

  @doc """
  Renders a single workingtime.
  """
  def show(%{workingtime: workingtime}) do
    %{data: data(workingtime)}
  end

  defp data(%WorkingTime{} = workingtime) do
    %{
        id: workingtime.id,
        start: workingtime.start,
        end: workingtime.end,
        user_id: workingtime.user_id
    }
  end

  defp errorCreate(%{changeset: changeset}) do
    %{
      error: "Erreur lors de l'insertion du workingTime",
      start: changeset.start,
      end: changeset.end
    }
  end
end

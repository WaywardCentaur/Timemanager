defmodule TimemanagerWeb.Router do
  use TimemanagerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {TimemanagerWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug CORSPlug, origin: "*"
  end

  pipeline :require_jwt do
    plug Guardian.Plug.VerifyHeader, module: Timemanager.Guardian, realm: "Bearer"
    plug Guardian.Plug.EnsureAuthenticated, error_handler: Guardian.Plug.ErrorHandler
  end

  scope "/", TimemanagerWeb do
    pipe_through :browser

    get "/", PageController, :home

  end

  #sans auth
  scope "/api", TimemanagerWeb do
    pipe_through :api

    scope "/users" do
      # Login
      post("/sign_in", UserController, :sign_in)
      # Create account
      post("/sign_up", UserController, :sign_up)
      # Disconnect
      post("/sign_out", UserController, :sign_out)
    end
  end


  #avec auth
  scope "/api", TimemanagerWeb do
    pipe_through [:api, :require_jwt]

    resources "/users", UserController
    scope "/workingtimes" do
      # create
      post("/:userID", WorkingTimeController, :create)
      # Get all of user
      get("/:userID", WorkingTimeController, :index)
      # Get specific timeframe
      get("/:userID/:workingTimeID", WorkingTimeController, :show)
      # update
      put("/:userID/:workingTimeID", WorkingTimeController, :update)
      # delete
      delete("/:userID/:workingTimeID", WorkingTimeController, :delete)
      options("/", WorkingTimeController, :options)
      options("/:userID", WorkingTimeController, :options)
    end
    scope "/clocks" do
      # create
      post("/:userID", ClockController, :create)
      # Get all of user
      get("/:userID", ClockController, :index)
      # update
      put("/:userID/:clockID", ClockController, :update)
      # delete
      delete("/:userID/:clockID", ClockController, :delete)
      options("/", ClockController, :options)
      options("/:userID", ClockController, :options)
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", TimemanagerWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:timemanager, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: TimemanagerWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end

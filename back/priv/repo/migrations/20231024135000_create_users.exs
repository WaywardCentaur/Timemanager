defmodule Timemanager.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :nom, :string
      add :prenom, :string
      add :role, :integer
      add :email, :string,  null: false
      add :hashed_password, :string, null: true
      add :username, :string, null: false
      add :mobile, :string
      add :profil, :string
      add :blocked, :boolean, default: false, null: false
      add :deleteOk, :boolean, default: false, null: false
      add :registeredAt, :utc_datetime
      add :lastLogin, :utc_datetime

      timestamps(type: :utc_datetime)
    end

    create unique_index(:users, [:email])

    create table(:users_tokens) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :token, :text, null: false
      add :context, :string, null: false
      add :sent_to, :string
      timestamps(type: :utc_datetime, updated_at: false)
    end

  end
end

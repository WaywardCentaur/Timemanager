# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Timemanager.Repo.insert!(%Timemanager.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.


timestamp = "2023-10-24T08:00:00Z"
parsed_datetime = Timex.parse!(timestamp, "{ISO:Extended:Z}")
utc_datetime = Timex.to_datetime(parsed_datetime, "Etc/UTC")

#passwords are "hihihihihihi"
Timemanager.Repo.insert_all(Timemanager.Userbase.User, [
  %{
    id: 901,
    nom: "nomuser1",
    prenom: "prenomuser1",
    username: "user1",
    blocked: false,
    mobile: "yes",
    role: 1,
    email: "email1@gmail.com",
    profil: "profileName",
    deleteOk: true,
    registeredAt: utc_datetime,
    lastLogin: utc_datetime,
    inserted_at: utc_datetime,
    updated_at: utc_datetime,
    hashed_password: "$2b$12$568NtxWo2A7Uqi8dM2s.GuXUAEFSyH3aY2ygRkx2395c7zaQ6vHJq"
  },
  %{
    id: 902,
    nom: "nomuser2",
    prenom: "prenomuser2",
    username: "user2",
    blocked: false,
    mobile: "yes",
    role: 1,
    email: "email2@gmail.com",
    profil: "profileName",
    deleteOk: true,
    registeredAt: utc_datetime,
    lastLogin: utc_datetime,
    inserted_at: utc_datetime,
    updated_at: utc_datetime,
    hashed_password: "$2b$12$568NtxWo2A7Uqi8dM2s.GuXUAEFSyH3aY2ygRkx2395c7zaQ6vHJq"
  },
  %{
    id: 903,
    nom: "nomuser3",
    prenom: "prenomuser3",
    username: "user3",
    blocked: false,
    mobile: "yes",
    role: 1,
    email: "email3@gmail.com",
    profil: "profileName",
    deleteOk: true,
    registeredAt: utc_datetime,
    lastLogin: utc_datetime,
    inserted_at: utc_datetime,
    updated_at: utc_datetime,
    hashed_password: "$2b$12$568NtxWo2A7Uqi8dM2s.GuXUAEFSyH3aY2ygRkx2395c7zaQ6vHJq"
  },
  %{
    id: 904,
    nom: "nomuser4",
    prenom: "prenomuser4",
    username: "user4",
    blocked: false,
    mobile: "yes",
    role: 1,
    email: "email4@gmail.com",
    profil: "profileName",
    deleteOk: true,
    registeredAt: utc_datetime,
    lastLogin: utc_datetime,
    inserted_at: utc_datetime,
    updated_at: utc_datetime,
    hashed_password: "$2b$12$568NtxWo2A7Uqi8dM2s.GuXUAEFSyH3aY2ygRkx2395c7zaQ6vHJq"
  }
])



{:ok, jwt1, _full_claims} = Timemanager.Guardian.encode_and_sign(Timemanager.Userbase.get_user!(901))
{:ok, jwt2, _full_claims} = Timemanager.Guardian.encode_and_sign(Timemanager.Userbase.get_user!(902))
{:ok, jwt3, _full_claims} = Timemanager.Guardian.encode_and_sign(Timemanager.Userbase.get_user!(903))
{:ok, jwt4, _full_claims} = Timemanager.Guardian.encode_and_sign(Timemanager.Userbase.get_user!(904))
Timemanager.Repo.insert_all(Timemanager.Userbase.UserToken, [
  %{
    id: 901,
    user_id: 901,
    token: jwt1,
    context: "register",
    sent_to: "user1",
    inserted_at: utc_datetime
  },
  %{
    id: 902,
    user_id: 902,
    token: jwt2,
    context: "register",
    sent_to: "user2",
    inserted_at: utc_datetime
  },
  %{
    id: 903,
    user_id: 903,
    token: jwt3,
    context: "register",
    sent_to: "user3",
    inserted_at: utc_datetime
  },
  %{
    id: 904,
    user_id: 904,
    token: jwt4,
    context: "register",
    sent_to: "user4",
    inserted_at: utc_datetime
  }
])


clock1_timestamp = "2023-10-24T08:00:00Z"
clock1_parsed_datetime = Timex.parse!(timestamp, "{ISO:Extended:Z}")
clock1_utc_datetime = Timex.to_datetime(parsed_datetime, "Etc/UTC")

clock2_timestamp = "2023-10-26T08:00:00Z"
clock2_parsed_datetime = Timex.parse!(timestamp, "{ISO:Extended:Z}")
clock2_utc_datetime = Timex.to_datetime(parsed_datetime, "Etc/UTC")

clock3_timestamp = "2023-10-28T08:00:00Z"
clock3_parsed_datetime = Timex.parse!(timestamp, "{ISO:Extended:Z}")
clock3_utc_datetime = Timex.to_datetime(parsed_datetime, "Etc/UTC")

clock4_timestamp = "2023-10-30T08:00:00Z"
clock4_parsed_datetime = Timex.parse!(timestamp, "{ISO:Extended:Z}")
clock4_utc_datetime = Timex.to_datetime(parsed_datetime, "Etc/UTC")

Timemanager.Repo.insert_all(Timemanager.Time.Clock, [
  #user1
  %{
    id: 901,
    user_id: 901,
    status: false,
    time: clock1_utc_datetime,
  },
  %{
    id: 902,
    user_id: 901,
    status: false,
    time: clock2_utc_datetime,
  },
  %{
    id: 903,
    user_id: 901,
    status: false,
    time: clock3_utc_datetime,
  },
  %{
    id: 904,
    user_id: 901,
    status: false,
    time: clock4_utc_datetime,
  },
  #user2
  %{
    id: 905,
    user_id: 902,
    status: false,
    time: clock1_utc_datetime,
  },
  %{
    id: 906,
    user_id: 902,
    status: false,
    time: clock2_utc_datetime
  },
  %{
    id: 907,
    user_id: 902,
    status: false,
    time: clock3_utc_datetime,
  },
  #user3
  %{
    id: 908,
    user_id: 903,
    status: false,
    time: clock1_utc_datetime,
  },
  %{
    id: 909,
    user_id: 903,
    status: false,
    time: clock2_utc_datetime,
  },
  #user4
  %{
    id: 910,
    user_id: 904,
    status: false,
    time: clock1_utc_datetime,
  },
])





start_clock1_timestamp = "2023-10-23T08:00:00Z"
start_clock1_parsed_datetime = Timex.parse!(timestamp, "{ISO:Extended:Z}")
start_clock1_utc_datetime = Timex.to_datetime(parsed_datetime, "Etc/UTC")

start_clock2_timestamp = "2023-10-25T08:00:00Z"
start_clock2_parsed_datetime = Timex.parse!(timestamp, "{ISO:Extended:Z}")
start_clock2_utc_datetime = Timex.to_datetime(parsed_datetime, "Etc/UTC")

start_clock3_timestamp = "2023-10-27T08:00:00Z"
start_clock3_parsed_datetime = Timex.parse!(timestamp, "{ISO:Extended:Z}")
start_clock3_utc_datetime = Timex.to_datetime(parsed_datetime, "Etc/UTC")

start_clock4_timestamp = "2023-10-29T08:00:00Z"
start_clock4_parsed_datetime = Timex.parse!(timestamp, "{ISO:Extended:Z}")
start_clock4_utc_datetime = Timex.to_datetime(parsed_datetime, "Etc/UTC")

Timemanager.Repo.insert_all(Timemanager.Time.WorkingTime, [
  #user1
  %{
    id: 901,
    user_id: 901,
    start: start_clock1_utc_datetime,
    end: clock1_utc_datetime,
  },
  %{
    id: 902,
    user_id: 901,
    start: start_clock2_utc_datetime,
    end: clock2_utc_datetime,
  },
  %{
    id: 903,
    user_id: 901,
    start: start_clock3_utc_datetime,
    end: clock3_utc_datetime,
  },
  %{
    id: 904,
    user_id: 901,
    start: start_clock4_utc_datetime,
    end: clock4_utc_datetime,
  },
  #user2
  %{
    id: 905,
    user_id: 902,
    start: start_clock1_utc_datetime,
    end: clock1_utc_datetime,
  },
  %{
    id: 906,
    user_id: 902,
    start: start_clock2_utc_datetime,
    end: clock2_utc_datetime,
  },
  %{
    id: 907,
    user_id: 902,
    start: start_clock3_utc_datetime,
    end: clock3_utc_datetime,
  },
  #user3
  %{
    id: 908,
    user_id: 903,
    start: start_clock1_utc_datetime,
    end: clock1_utc_datetime,
  },
  %{
    id: 909,
    user_id: 903,
    start: start_clock2_utc_datetime,
    end: clock2_utc_datetime,
  },
  #user4
  %{
    id: 910,
    user_id: 904,
    start: start_clock1_utc_datetime,
    end: clock1_utc_datetime,
  },
])

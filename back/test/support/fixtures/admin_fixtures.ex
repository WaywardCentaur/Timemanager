defmodule Timemanager.AdminFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Timemanager.Admin` context.
  """

  @doc """
  Generate a unique user email.
  """
  def unique_user_email, do: "some email#{System.unique_integer([:positive])}"

  @doc """
  Generate a user.
  """
  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{
        blocked: true,
        deleteOk: true,
        email: unique_user_email(),
        lastLogin: ~U[2023-10-22 12:30:00Z],
        mail: "some mail",
        mobile: "some mobile",
        nom: "some nom",
        prenom: "some prenom",
        profil: "some profil",
        registeredAt: ~U[2023-10-22 12:30:00Z],
        role: 42
      })
      |> Timemanager.Admin.create_user()

    user
  end

  @doc """
  Generate a time.
  """
  def time_fixture(attrs \\ %{}) do
    {:ok, time} =
      attrs
      |> Enum.into(%{
        status: true,
        time: ~U[2023-10-22 13:22:00Z],
        user: "some user"
      })
      |> Timemanager.Admin.create_time()

    time
  end

  @doc """
  Generate a intervals.
  """
  def intervals_fixture(attrs \\ %{}) do
    {:ok, intervals} =
      attrs
      |> Enum.into(%{
        end: ~U[2023-10-22 13:36:00Z],
        start: ~U[2023-10-22 13:36:00Z],
        user: "some user"
      })
      |> Timemanager.Admin.create_intervals()

    intervals
  end
end

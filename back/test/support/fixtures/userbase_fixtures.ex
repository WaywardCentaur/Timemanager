defmodule Timemanager.UserbaseFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Timemanager.Userbase` context.
  """

  @doc """
  Generate a user.
  """
  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{

      })
      |> Timemanager.Userbase.create_user()

    user
  end
end

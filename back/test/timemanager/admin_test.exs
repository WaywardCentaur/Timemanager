defmodule Timemanager.AdminTest do
  use Timemanager.DataCase

  alias Timemanager.Admin

  describe "users" do
    alias Timemanager.Admin.User

    import Timemanager.AdminFixtures

    @invalid_attrs %{blocked: nil, mobile: nil, role: nil, mail: nil, nom: nil, prenom: nil, email: nil, profil: nil, deleteOk: nil, registeredAt: nil, lastLogin: nil}

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Admin.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Admin.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      valid_attrs = %{blocked: true, mobile: "some mobile", role: 42, mail: "some mail", nom: "some nom", prenom: "some prenom", email: "some email", profil: "some profil", deleteOk: true, registeredAt: ~U[2023-10-22 12:30:00Z], lastLogin: ~U[2023-10-22 12:30:00Z]}

      assert {:ok, %User{} = user} = Admin.create_user(valid_attrs)
      assert user.blocked == true
      assert user.mobile == "some mobile"
      assert user.role == 42
      assert user.mail == "some mail"
      assert user.nom == "some nom"
      assert user.prenom == "some prenom"
      assert user.email == "some email"
      assert user.profil == "some profil"
      assert user.deleteOk == true
      assert user.registeredAt == ~U[2023-10-22 12:30:00Z]
      assert user.lastLogin == ~U[2023-10-22 12:30:00Z]
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Admin.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      update_attrs = %{blocked: false, mobile: "some updated mobile", role: 43, mail: "some updated mail", nom: "some updated nom", prenom: "some updated prenom", email: "some updated email", profil: "some updated profil", deleteOk: false, registeredAt: ~U[2023-10-23 12:30:00Z], lastLogin: ~U[2023-10-23 12:30:00Z]}

      assert {:ok, %User{} = user} = Admin.update_user(user, update_attrs)
      assert user.blocked == false
      assert user.mobile == "some updated mobile"
      assert user.role == 43
      assert user.mail == "some updated mail"
      assert user.nom == "some updated nom"
      assert user.prenom == "some updated prenom"
      assert user.email == "some updated email"
      assert user.profil == "some updated profil"
      assert user.deleteOk == false
      assert user.registeredAt == ~U[2023-10-23 12:30:00Z]
      assert user.lastLogin == ~U[2023-10-23 12:30:00Z]
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Admin.update_user(user, @invalid_attrs)
      assert user == Admin.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Admin.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Admin.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Admin.change_user(user)
    end
  end

  describe "clock" do
    alias Timemanager.Admin.Time

    import Timemanager.AdminFixtures

    @invalid_attrs %{status: nil, time: nil, user: nil}

    test "list_clock/0 returns all clock" do
      time = time_fixture()
      assert Admin.list_clock() == [time]
    end

    test "get_time!/1 returns the time with given id" do
      time = time_fixture()
      assert Admin.get_time!(time.id) == time
    end

    test "create_time/1 with valid data creates a time" do
      valid_attrs = %{status: true, time: ~U[2023-10-22 13:22:00Z], user: "some user"}

      assert {:ok, %Time{} = time} = Admin.create_time(valid_attrs)
      assert time.status == true
      assert time.time == ~U[2023-10-22 13:22:00Z]
      assert time.user == "some user"
    end

    test "create_time/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Admin.create_time(@invalid_attrs)
    end

    test "update_time/2 with valid data updates the time" do
      time = time_fixture()
      update_attrs = %{status: false, time: ~U[2023-10-23 13:22:00Z], user: "some updated user"}

      assert {:ok, %Time{} = time} = Admin.update_time(time, update_attrs)
      assert time.status == false
      assert time.time == ~U[2023-10-23 13:22:00Z]
      assert time.user == "some updated user"
    end

    test "update_time/2 with invalid data returns error changeset" do
      time = time_fixture()
      assert {:error, %Ecto.Changeset{}} = Admin.update_time(time, @invalid_attrs)
      assert time == Admin.get_time!(time.id)
    end

    test "delete_time/1 deletes the time" do
      time = time_fixture()
      assert {:ok, %Time{}} = Admin.delete_time(time)
      assert_raise Ecto.NoResultsError, fn -> Admin.get_time!(time.id) end
    end

    test "change_time/1 returns a time changeset" do
      time = time_fixture()
      assert %Ecto.Changeset{} = Admin.change_time(time)
    end
  end

  describe "workingtimes" do
    alias Timemanager.Admin.Intervals

    import Timemanager.AdminFixtures

    @invalid_attrs %{start: nil, user: nil, end: nil}

    test "list_workingtimes/0 returns all workingtimes" do
      intervals = intervals_fixture()
      assert Admin.list_workingtimes() == [intervals]
    end

    test "get_intervals!/1 returns the intervals with given id" do
      intervals = intervals_fixture()
      assert Admin.get_intervals!(intervals.id) == intervals
    end

    test "create_intervals/1 with valid data creates a intervals" do
      valid_attrs = %{start: ~U[2023-10-22 13:36:00Z], user: "some user", end: ~U[2023-10-22 13:36:00Z]}

      assert {:ok, %Intervals{} = intervals} = Admin.create_intervals(valid_attrs)
      assert intervals.start == ~U[2023-10-22 13:36:00Z]
      assert intervals.user == "some user"
      assert intervals.end == ~U[2023-10-22 13:36:00Z]
    end

    test "create_intervals/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Admin.create_intervals(@invalid_attrs)
    end

    test "update_intervals/2 with valid data updates the intervals" do
      intervals = intervals_fixture()
      update_attrs = %{start: ~U[2023-10-23 13:36:00Z], user: "some updated user", end: ~U[2023-10-23 13:36:00Z]}

      assert {:ok, %Intervals{} = intervals} = Admin.update_intervals(intervals, update_attrs)
      assert intervals.start == ~U[2023-10-23 13:36:00Z]
      assert intervals.user == "some updated user"
      assert intervals.end == ~U[2023-10-23 13:36:00Z]
    end

    test "update_intervals/2 with invalid data returns error changeset" do
      intervals = intervals_fixture()
      assert {:error, %Ecto.Changeset{}} = Admin.update_intervals(intervals, @invalid_attrs)
      assert intervals == Admin.get_intervals!(intervals.id)
    end

    test "delete_intervals/1 deletes the intervals" do
      intervals = intervals_fixture()
      assert {:ok, %Intervals{}} = Admin.delete_intervals(intervals)
      assert_raise Ecto.NoResultsError, fn -> Admin.get_intervals!(intervals.id) end
    end

    test "change_intervals/1 returns a intervals changeset" do
      intervals = intervals_fixture()
      assert %Ecto.Changeset{} = Admin.change_intervals(intervals)
    end
  end
end

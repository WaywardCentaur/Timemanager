defmodule TimemanagerWeb.WorkingTimeControllerTest do
  use TimemanagerWeb.ConnCase

  import Timemanager.TimeFixtures

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  describe "index" do
    test "lists all workingtimes", %{conn: conn} do
      conn = get(conn, ~p"/workingtimes")
      assert html_response(conn, 200) =~ "Listing Workingtimes"
    end
  end

  describe "new working_time" do
    test "renders form", %{conn: conn} do
      conn = get(conn, ~p"/workingtimes/new")
      assert html_response(conn, 200) =~ "New Working time"
    end
  end

  describe "create working_time" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/workingtimes", working_time: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == ~p"/workingtimes/#{id}"

      conn = get(conn, ~p"/workingtimes/#{id}")
      assert html_response(conn, 200) =~ "Working time #{id}"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/workingtimes", working_time: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Working time"
    end
  end

  describe "edit working_time" do
    setup [:create_working_time]

    test "renders form for editing chosen working_time", %{conn: conn, working_time: working_time} do
      conn = get(conn, ~p"/workingtimes/#{working_time}/edit")
      assert html_response(conn, 200) =~ "Edit Working time"
    end
  end

  describe "update working_time" do
    setup [:create_working_time]

    test "redirects when data is valid", %{conn: conn, working_time: working_time} do
      conn = put(conn, ~p"/workingtimes/#{working_time}", working_time: @update_attrs)
      assert redirected_to(conn) == ~p"/workingtimes/#{working_time}"

      conn = get(conn, ~p"/workingtimes/#{working_time}")
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, working_time: working_time} do
      conn = put(conn, ~p"/workingtimes/#{working_time}", working_time: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Working time"
    end
  end

  describe "delete working_time" do
    setup [:create_working_time]

    test "deletes chosen working_time", %{conn: conn, working_time: working_time} do
      conn = delete(conn, ~p"/workingtimes/#{working_time}")
      assert redirected_to(conn) == ~p"/workingtimes"

      assert_error_sent 404, fn ->
        get(conn, ~p"/workingtimes/#{working_time}")
      end
    end
  end

  defp create_working_time(_) do
    working_time = working_time_fixture()
    %{working_time: working_time}
  end
end

defmodule TimemanagerWeb.WorkingTimesControllerTest do
  use TimemanagerWeb.ConnCase

  import Timemanager.TimeFixtures

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  describe "index" do
    test "lists all workingtime", %{conn: conn} do
      conn = get(conn, ~p"/workingtime")
      assert html_response(conn, 200) =~ "Listing Workingtime"
    end
  end

  describe "new working_times" do
    test "renders form", %{conn: conn} do
      conn = get(conn, ~p"/workingtime/new")
      assert html_response(conn, 200) =~ "New Working times"
    end
  end

  describe "create working_times" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/workingtime", working_times: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == ~p"/workingtime/#{id}"

      conn = get(conn, ~p"/workingtime/#{id}")
      assert html_response(conn, 200) =~ "Working times #{id}"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/workingtime", working_times: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Working times"
    end
  end

  describe "edit working_times" do
    setup [:create_working_times]

    test "renders form for editing chosen working_times", %{conn: conn, working_times: working_times} do
      conn = get(conn, ~p"/workingtime/#{working_times}/edit")
      assert html_response(conn, 200) =~ "Edit Working times"
    end
  end

  describe "update working_times" do
    setup [:create_working_times]

    test "redirects when data is valid", %{conn: conn, working_times: working_times} do
      conn = put(conn, ~p"/workingtime/#{working_times}", working_times: @update_attrs)
      assert redirected_to(conn) == ~p"/workingtime/#{working_times}"

      conn = get(conn, ~p"/workingtime/#{working_times}")
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, working_times: working_times} do
      conn = put(conn, ~p"/workingtime/#{working_times}", working_times: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Working times"
    end
  end

  describe "delete working_times" do
    setup [:create_working_times]

    test "deletes chosen working_times", %{conn: conn, working_times: working_times} do
      conn = delete(conn, ~p"/workingtime/#{working_times}")
      assert redirected_to(conn) == ~p"/workingtime"

      assert_error_sent 404, fn ->
        get(conn, ~p"/workingtime/#{working_times}")
      end
    end
  end

  defp create_working_times(_) do
    working_times = working_times_fixture()
    %{working_times: working_times}
  end
end

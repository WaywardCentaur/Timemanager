args=("$@")

echo 'Waiting for containers to shutdown...'
docker stop $(docker ps -a -q)
echo 'Removing containers ...'
docker container rm $(docker container ps -a -q) --force
sleep 5
if [ ${#args[@]} -eq 0 ] || [ ${#args[0]} -ne '--keepdata' -o ${#args[0]} -ne '-kd' ]; then
	docker volume rm $(docker volume ls -q)
fi
docker-compose up -d --build

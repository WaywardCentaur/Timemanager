// rollup.config.js
import resolve from '@rollup/plugin-node-resolve';

export default {
  input: 'src/your_entry_file.js', // Adjust the entry file path based on your project structure
  output: {
    file: 'dist/bundle.js', // Adjust the output file path as needed
    format: 'es', // Output format: 'es' for ES modules, 'cjs' for CommonJS
  },
  plugins: [
    resolve(), // This plugin helps Rollup to locate modules in node_modules
    // Add other plugins if needed
  ],
};
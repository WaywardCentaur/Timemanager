/**
 * custom typings so typescript knows about the schema-fields
 */

import { RxDocument, RxCollection, RxDatabase } from 'rxdb';

export interface RxHeroDocumentType {
    name: string;
    color: string;
    maxHP: number;
    hp: number;
    team?: string;
    skills: Array<{
        name?: string,
        damage?: string
    }>;
}

// ORM methods
interface RxHeroDocMethods {
    hpPercent(): number;
}

export type RxHeroDocument = RxDocument<RxHeroDocumentType, RxHeroDocMethods>;

export type RxHeroCollection = RxCollection<RxHeroDocumentType, RxHeroDocMethods, {}>;

export interface RxHeroesCollections {
    heroes: RxHeroCollection;
}

export type RxHeroesDatabase = RxDatabase<RxHeroesCollections>;
//////////////////////User Schema/////////////////////////
export interface RxUserDocumentType {
    firstname: string;
    lastname: string;
    team?: string;
    email?: string;
    skills: Array<{
        name?: string
    }>;
}

// ORM methods
interface RxUserDocMethods {
    hpPercent(): number;
    isLoggedin(): boolean;
}

export type RxUserDocument = RxDocument<RxUserDocumentType, RxUserDocMethods>;

export type RxUserCollection = RxCollection<RxUserDocumentType, RxUserDocMethods, {}>;

export interface RxUsersCollections {
    users: RxUserCollection;
}

export type RxUsersDatabase = RxDatabase<RxUsersCollections>;

//////////////////////Skill Schema/////////////////////////

export interface RxSkillDocumentType {
    name: string;
    code: string;
}

// ORM methods
interface RxSkillDocMethods {
    isActive(): boolean;
}

export type RxSkillDocument = RxDocument<RxSkillDocumentType, RxSkillDocMethods>;

export type RxSkillCollection = RxCollection<RxSkillDocumentType, RxSkillDocMethods, {}>;

export interface RxSkillsCollections {
    skills: RxSkillCollection;
}

export type RxSkillsDatabase = RxDatabase<RxSkillsCollections>;
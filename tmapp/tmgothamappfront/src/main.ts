/**
 * main.ts
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Plugins
import { registerPlugins } from '@/plugins'

// Components
import App from './App.vue'

// Composables
import { createApp } from 'vue'
import { createDatabase } from './database';
import { createI18n } from 'vue-i18n'

const i18n = createI18n({
  // something vue-i18n options here ...
})
const database = createDatabase();
const app = createApp(App)
app.use(i18n)
registerPlugins(app)

database.then(db => {
    app.use(db).mount('#app');
  });

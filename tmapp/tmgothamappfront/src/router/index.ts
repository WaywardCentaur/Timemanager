// Composables
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/dashboard:username',
    name: 'Dashboard',
    component: () => import('@/components/Dashboard.vue'),
    props: {},
    children: [
      
      {
        path: "",
        name: "Task",
        component: () => import('@/views/TasksList.vue'),
        
    },
    {
      path: "taskdetail:taskName",
      name: "taskDetail",
      component: () => import('@/views/TaskDetail.vue'),
      props: {}
    },
    {
        path: "profile",
        name: "Profile",
        component: () => import('@/views/Profile.vue'),
     },
    {
      path: "skilllist",
      name: "SkillList",
      component: () => import('@/views/SkillList.vue')
    },
     {
         path: "userlist",
         name: "UserList",
         component: () => import('@/views/UserList.vue'),  

      },
      {
        path: "userdetail/:userName",
        name: "userDetail",
        component: () => import('@/views/UserDetail.vue'),
        props: {}
      }
    ],
  },
  {
    path: '/',
    name: 'login',
    component: () => import('@/components/Login.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router

import { RxSkillDocumentType } from '@/RxDB';
import { RxJsonSchema } from 'rxdb';
const schema: RxJsonSchema<RxSkillDocumentType> = {
  title: 'skill schema',
  description: 'a skill',
  version: 0,
  keyCompression: false,
  primaryKey: 'name',
  type: 'object',
  properties: {
    name: {
      type: 'string',
      default: '',
      maxLength: 100,
    },
    code: {
      type: 'string',
      default: '',
      maxLength: 100,
    },
   
  },
  required: ['name', 'code'],
};

export default schema;

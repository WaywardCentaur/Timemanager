import { RxUserDocumentType } from '@/RxDB';
import { RxJsonSchema } from 'rxdb';
const schema: RxJsonSchema<RxUserDocumentType> = {
  title: 'user schema',
  description: 'describes a user',
  version: 0,
  keyCompression: false,
  primaryKey: 'email',
  type: 'object',
  properties: {
    firstname: {
      type: 'string',
      default: '',
      maxLength: 100,
    },
    lastname: {
        type: 'string',
        default: '',
        maxLength: 100,
    },
    email: {
        type: 'string',
        default: '',
        maxLength: 100,
    },  
    team: {
      description: 'color of the team this hero belongs to',
      type: 'string',
    },
    skills: {
      type: 'array',
      maxItems: 5,
      uniqueItems: true,
      items: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
          },
          damage: {
            type: 'number',
          },
        },
      },
      default: [],
    },
  },
  required: ['firstname', 'lastname', 'email'],
};

export default schema;